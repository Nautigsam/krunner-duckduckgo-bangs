# krunner-duckduckgo-bangs

A little krunner plugin (Plasma 5) to execute DuckDuckGo bangs.

E.g. you can type `!yt` and krunner will propose to open your default browser and call a DuckDuckGo URL that will resolve the bang to Youtube.

### How to install
```
git clone https://Nautigsam@framagit.org/Nautigsam/krunner-duckduckgo-bangs.git
cd krunner-duckduckgo-bangs
mkdir build
cd build
cmake ..
make
sudo make install
kquitapp5 krunner
kstart5 --windowclass krunner krunner > /dev/null 2>&1 &
```