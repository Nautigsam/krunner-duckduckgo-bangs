#include <iostream>

#include <KLocalizedString>
#include <KConfigCore/KConfig>
#include <KConfigCore/KConfigGroup>

#include <QApplication>

#include "ddg_bangs.h"

using namespace std;

DDGBangs::DDGBangs(QObject *parent, const QVariantList &args)
    : Plasma::AbstractRunner(parent, args)
{
    Q_UNUSED(args);

    // General runner configuration
    setObjectName(QLatin1String("DuckDuckGo bangs"));
    setHasRunOptions(true);
    setIgnoredTypes(Plasma::RunnerContext::Directory |
                    Plasma::RunnerContext::File |
                    Plasma::RunnerContext::NetworkLocation);
    setSpeed(AbstractRunner::NormalSpeed);
    setPriority(HighestPriority);
}

DDGBangs::~DDGBangs() {}

void DDGBangs::match(Plasma::RunnerContext &context)
{
    if (!context.isValid()) return;

    const QString enteredKey = context.query();
    
    QList<Plasma::QueryMatch> matches;

    if (enteredKey.startsWith('!') && enteredKey.length() >= 2) {
        // create match object
        Plasma::QueryMatch match(this);
        match.setText(enteredKey);
        match.setSubtext("Open in DuckDuckGo");
        match.setData("bang");

        matches.append(match);
    }

    // Feed the framework with the calculated results
    context.addMatches(matches);
}

/**
 * Perform an action when a user chooses one of the previously found matches.
 * Either some string gets copied to the clipboard, a file/path/URL is being opened, 
 * or a command is being executed.
 */
void DDGBangs::run(const Plasma::RunnerContext &context, const Plasma::QueryMatch &match)
{
    Q_UNUSED(context);

    string command = "kde-open5 https://duckduckgo.com/?q=" + QUrl::toPercentEncoding(match.text()).toStdString() + " &";
    system(command.c_str());
}

K_EXPORT_PLASMA_RUNNER(ddg_bangs, DDGBangs)

#include "ddg_bangs.moc"
