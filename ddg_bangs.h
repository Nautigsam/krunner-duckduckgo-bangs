#ifndef DDG_BANGS_H
#define DDG_BANGS_H

#include <KRunner/AbstractRunner>

class DDGBangs : public Plasma::AbstractRunner
{
    Q_OBJECT

public:
    DDGBangs(QObject *parent, const QVariantList &args);
    ~DDGBangs();

    void match(Plasma::RunnerContext &);
    void run(const Plasma::RunnerContext &, const Plasma::QueryMatch &);
};

#endif // DDG_BANGS_H
